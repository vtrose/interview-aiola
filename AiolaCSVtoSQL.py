#!/usr/bin/python
import glob
import os
import pandas as pd
import pyodbc
import pandasql as ps

conn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER=VITARO-LAP;DATABASE=Local;Trusted_Connection=yes;')

cur = conn.cursor()
# file_path = "/home/ec2-user/pre-omop/chameleon/txt/"
# file_path = os.path.realpath(__file__).replace("createSQLtable.py","") + "txt/"
file_path = os.path.realpath(__file__).replace(os.path.basename(os.path.realpath(__file__)),"") + "csv\\"

# Loop through each CSV
for filename in glob.glob(file_path+"*.csv"):
# Create a table name
    tablename = filename.replace(file_path, "").replace(".csv", "")
    df= pd.read_csv(filename,encoding = "utf-8")
    data_top = df.head(0)
    # columnsSelect = str(df.head(0))
    # columnsSelect = columnsSelect.replace('Empty DataFrame\nColumns: [','').replace(']\nIndex: []','')
    columns = ''
    for column in data_top:
        columns = columns +'[' + column + ']' + ' varchar (255),'
    # print(type(data_top)
    print(columns)
    columns = '('+columns[:-1]+')'
    sqlQueryCreate = 'drop table if exists dbo.'+ tablename +';  CREATE TABLE dbo.'+ tablename + columns
    cur.execute(sqlQueryCreate) 
    conn.commit()
    cur.close()
    cur = conn.cursor()
#column names into a string separated by commas
    colSelect = ",".join(['['+str(i)+']' for i in df.columns.tolist()])
    for i,row in df.iterrows():
        row[1]
        sql = "INSERT INTO " + tablename + " (" +colSelect + ") VALUES ('"+"','".join([str(i).replace("\'","\''") for i in row])+"')"
        # word = word.replace("\'","\''")
        print(sql)
        cur.execute(sql)
    conn.commit()
cur.close()
    # print(sqlQueryCreate)
   
