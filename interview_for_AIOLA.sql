-----Q1
select top 3 count([CHFLTN]) as cnt, [CHLOCCT]
from(
select *
from [dbo].[biztalkfile]
where chstol = chptol
)a
group by [CHLOCCT]
order by count([CHFLTN]) desc

-----Q2
select *
from [dbo].[biztalkfile] a join [dbo].[countries8] b
on a.[CHLOCCT]=b.country
where [CHRMINE]='LANDED'
and cast(REPLACE(replace([Area in km],'nan',''), ',', '') as float)> 1000000 


-----Q3
--For all countries with population greater than 10M and the num of flights are less than
--50: Show name of country, number flights, the diff to next record number of flight
select *, cnt - LAG(cnt) OVER (ORDER BY cnt ) AS difference
from (
select [CHLOCCT], count([CHFLTN]) cnt from
(
select * from [dbo].[biztalkfile] a join [dbo].[countries8] b on a.[CHLOCCT]=b.country
where cast(REPLACE(replace([Population],'nan',''), ',', '') as float)> 10000000 
and [CHLOCCT] in 
(select [CHLOCCT] from [dbo].[biztalkfile] group by [CHLOCCT] having count([CHFLTN])<50)
)a
group by [CHLOCCT]
order by count([CHFLTN]) desc
)aa

